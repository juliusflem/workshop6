punc = '''!()-[]{};:'"\, <>./?@#$%^&*_~'''

text = 'How much wood would a woodchuck chuck if a woodchuck could chuck wood.'
text = text.split()
results = {}
resultlist = []
for word in text:
    word = word.upper()
    if word not in results:
        for char in word:
            if char in punc:
                word = word.replace(char, '')
        results[word] = 1
    else:
        results[word] += 1
results = results.items()
for key, value in results:
    resultlist.append((key, value))
resultlist.sort(key=lambda tup: tup[1], reverse=True)
for (key, value) in resultlist:
    print('{} {}'.format(key, value))
